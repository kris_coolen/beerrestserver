package be.kriscoolen.spring.restserver_beers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidBeerException extends RuntimeException {

    public InvalidBeerException(String message){
        super(message);
    }
}
