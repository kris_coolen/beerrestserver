package be.kriscoolen.spring.restserver_beers.repository;

import be.kriscoolen.spring.restserver_beers.domain.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeerOrderRepository extends JpaRepository<BeerOrder, Integer> {
    public default int saveOrder(BeerOrder order){
        saveAndFlush(order);
        return order.getId();
    }
    public default BeerOrder getBeerOrderById(int id){
        return findById(id).orElse(null);
    }

    public default List<BeerOrder> getAllBeerOrders(){
        return findAll();
    }
}
