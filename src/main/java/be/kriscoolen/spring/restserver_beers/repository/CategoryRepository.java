package be.kriscoolen.spring.restserver_beers.repository;


import be.kriscoolen.spring.restserver_beers.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    public default Category findCategoryById(Integer id){
        return findById(id).orElse(null);
    }

}
