package be.kriscoolen.spring.restserver_beers.restcontroller;

import be.kriscoolen.spring.restserver_beers.domain.Beer;
import be.kriscoolen.spring.restserver_beers.domain.BeerOrder;
import be.kriscoolen.spring.restserver_beers.domain.Order;
import be.kriscoolen.spring.restserver_beers.exceptions.InvalidBeerException;
import be.kriscoolen.spring.restserver_beers.repository.BeerOrderRepository;
import be.kriscoolen.spring.restserver_beers.service.BeerService;
import be.kriscoolen.spring.restserver_beers.wrapper.OrderList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value="/orders")
public class BeerOrdersRestController {

    @Autowired
    private BeerService beerService;

    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @GetMapping(value = "{id}",produces= {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<BeerOrder> getBeerOrder(@PathVariable("id") int id){
        BeerOrder beerOrder = beerOrderRepository.getBeerOrderById(id);
        if(beerOrder!=null){
            return new ResponseEntity<>(beerOrder, HttpStatus.OK);
        }else{
            throw new EntityNotFoundException("beer order with id "+id+" not found!");
        }
    }

    @GetMapping(produces= {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<OrderList> getOrders(){
        List<BeerOrder> orders = beerOrderRepository.getAllBeerOrders();
        OrderList orderList = new OrderList(orders);
        return new ResponseEntity<>(orderList,HttpStatus.OK);
    }

    /*@PostMapping(consumes={MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addOrder(@RequestBody Order order, HttpServletRequest request){

        int orderId = beerService.orderBeer(order.getOrderName(),order.getBeerId(),order.getQuantity());
        URI uri = URI.create(request.getRequestURL()+"/"+orderId);
        return ResponseEntity.created(uri).build();
    }*/
    @PostMapping(consumes={MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addOrder(@RequestBody Order order, HttpServletRequest request){
        int orderSize = order.getBeerIdList().size();
        int[][] orderArray = new int[orderSize][2];
        for(int i=0; i<orderSize;i++){
            orderArray[i][0] = order.getBeerIdList().get(i);
            orderArray[i][1] = order.getQuantityList().get(i);
        }
        int orderId=0;
        try {
            orderId = beerService.orderBeers(order.getOrderName(), orderArray);
        }catch(InvalidBeerException ibe){
            throw new InvalidBeerException(ibe.getMessage());
        }
        URI uri = URI.create(request.getRequestURL()+"/"+orderId);
        return ResponseEntity.created(uri).build();
    }


}
