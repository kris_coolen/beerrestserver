package be.kriscoolen.spring.restserver_beers.restcontroller;

import be.kriscoolen.spring.restserver_beers.domain.Beer;
import be.kriscoolen.spring.restserver_beers.repository.BeerRepository;
import be.kriscoolen.spring.restserver_beers.wrapper.BeerList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping(value="/beers")
public class BeersRestController {
    @Autowired
    private BeerRepository beerRepository;
    @GetMapping(value = "{id}",produces= {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Beer> getBeer(@PathVariable("id") int id){
        Beer beer = beerRepository.getBeerById(id);
        if(beer!=null){
            return new ResponseEntity<>(beer, HttpStatus.OK);
        }else{
            throw new EntityNotFoundException("Beer with id " + id + " not found!");
        }
    }
    @GetMapping(produces= {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE},
                params={"alcohol"})
    public ResponseEntity<BeerList> getBeersForAlcohol(@RequestParam("alcohol") float alcohol){
        List<Beer> beers = beerRepository.getBeersByAlcohol(alcohol);
        BeerList beerList = new BeerList(beers);
        return new ResponseEntity<>(beerList,HttpStatus.OK);

    }
    @GetMapping(produces= {MediaType.APPLICATION_JSON_UTF8_VALUE,MediaType.APPLICATION_XML_VALUE},
            params={"name"})
    public ResponseEntity<BeerList> getBeersContainingName(@RequestParam("name") String name){
        List<Beer> beers = beerRepository.findBeersByNameIgnoreCaseContainingOrderByNameAsc(name);
        BeerList beerList = new BeerList(beers);
        return new ResponseEntity<>(beerList,HttpStatus.OK);

    }
}
