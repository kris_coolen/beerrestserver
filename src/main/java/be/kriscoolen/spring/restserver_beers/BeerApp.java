package be.kriscoolen.spring.restserver_beers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
//        (
//                exclude = org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
//        )
public class BeerApp {

    public static void main(String[] args) {
        SpringApplication.run(BeerApp.class, args);
    }

        @Bean
        public WebSecurityConfigurer<WebSecurity> securityConfigurer() {
            return new WebSecurityConfigurerAdapter() {
                @Override
                protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                    auth.inMemoryAuthentication()
                        .passwordEncoder(new BCryptPasswordEncoder())
                        .withUser("homer")
                        .password("$2a$10$6ijNwwL19abb5t/kD2AqLeMYi8/fNuldNSSrU9h6CLHEhAWW6IB.S")
                        .roles("ADULT")
                        .and()
                        .withUser("bart")
                        .password("$2a$10$6ijNwwL19abb5t/kD2AqLeMYi8/fNuldNSSrU9h6CLHEhAWW6IB.S")
                        .roles("MINOR");
                }

                @Override
                protected void configure(HttpSecurity http) throws Exception {
                    http.csrf().disable();
                    http.httpBasic();
                    http.authorizeRequests().antMatchers("/orders/**").hasRole("ADULT");

                }
            };

        }
}



