package be.kriscoolen.spring.restserver_beers.service;


import be.kriscoolen.spring.restserver_beers.exceptions.InvalidBeerException;
import be.kriscoolen.spring.restserver_beers.exceptions.InvalidNumberException;

public interface BeerService {
    public int orderBeer(String name, int beerId, int number) throws InvalidBeerException, InvalidNumberException;
    public int orderBeers(String name, int[][] order) throws InvalidBeerException, InvalidNumberException;
}
