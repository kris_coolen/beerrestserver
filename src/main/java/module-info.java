open module BeersRestServer {
    requires spring.core;
    requires spring.context;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires java.sql;
    requires spring.boot.starter.jdbc;
    requires spring.jdbc;
    requires java.persistence;
    requires spring.boot.starter.data.jpa;
    requires spring.data.jpa;
    requires spring.data.commons;
    requires spring.tx;
    requires spring.boot.starter.security;
    requires spring.security.config;
    requires spring.security.core;
    requires net.bytebuddy;

    //REST
    requires spring.web;
    //JEE annotations
    requires java.annotation;
    //XML binding
    requires java.xml.bind;
    requires com.sun.xml.bind;
    //JSON binding
    requires jackson.annotations;
    //Tomcat JEE classes
    requires tomcat.embed.core;
    //bean validation API
    requires java.validation;
}