package be.kriscoolen.spring.restserver_beers.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
@Transactional
public class CategoryRepositoryTest {

    @Autowired
    CategoryRepository categoryRepository;


    @Test
    public void findCategoryByIdTest(){
        assertNotNull(categoryRepository.findCategoryById(1));
        assertNull(categoryRepository.findCategoryById(0));
    }

}
