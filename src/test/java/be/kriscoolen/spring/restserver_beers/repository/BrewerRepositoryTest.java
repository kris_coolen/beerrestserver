package be.kriscoolen.spring.restserver_beers.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
@Transactional
public class BrewerRepositoryTest {

    @Autowired
    BrewerRepository brewerRepository;

    @Test
    public void getBrewerByIdTest(){
        assertNotNull(brewerRepository.findBrewerById(1));
        assertNull(brewerRepository.findBrewerById(0));
    }
}
