TRUNCATE TABLE Brewers;
TRUNCATE TABLE Categories;
TRUNCATE TABLE Beers;
TRUNCATE TABLE BeerOrders;
TRUNCATE TABLE BeerOrderItems;

INSERT INTO Brewers VALUES(1,'TestBrewer','TestStreet',1000,'TestCity',10000);
INSERT INTO Brewers VALUES(2,'Kris Coolen','Moorskampweg',3680,'Maaseik',1000);
INSERT INTO Brewers VALUES(3,'Omar Schultink beers','Janettenstraat',3920,'Achel',10000);

INSERT INTO Categories VALUES (1,'TestCategory');
INSERT INTO Categories VALUES(2,'Piswater');
INSERT INTO Beers VALUES (1,'chimay Triple',2,1,2.75,100,7,0,NULL);
INSERT INTO Beers VALUES (2,'Chimay Rouge',2,1,3.75,100,7,0,NULL);
INSERT INTO Beers VALUES (3,'Blue Chimay Grand Cru',2,1,4.75,50,6.5,0,NULL);
INSERT INTO Beers VALUES (4,'Westmalle trippel',1,1,5.75,10,6.5,0,NULL);
INSERT INTO Beers VALUES(5,'St Bernardus Abt',3,2,5.0,200,9.0,0,NULL);
INSERT INTO Beers VALUES(6,'St. Gummarus',3,2,4.30,1000,9,0,NULL);
INSERT INTO Beers VALUES(7,'Chimay Blue',2,1,5.30,1000,9,0,NULL);
INSERT INTO Beers VALUES(8,'St. Nicolaas bier',1,1,2.2,250,8.5,0,NULL);
INSERT INTO BeerOrders VALUES(1,'Bestelling kris');
INSERT INTO BeerOrderItems VALUES (1,24,3,1);
INSERT INTO BeerOrderItems VALUES (2,6,4,1);